export default () => {
    const openMenuButton = document.getElementById("open-menu-button");
    const exitMenuButton = document.getElementById("exit-menu-button")
    const menu = document.getElementById("menu");

    openMenuButton.addEventListener("click", () => {
        menu.classList.add("header-navigation__wrap-menu--active");
    });

    exitMenuButton.addEventListener("click", () => {
        menu.classList.remove("header-navigation__wrap-menu--active")
    });
}

