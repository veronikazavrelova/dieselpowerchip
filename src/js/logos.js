const MARGIN_MOVE = 300;

export default () => {
    const logosWrap = document.getElementById("logos-wrap");
    const logos = document.getElementById("logos");
    const arrowLeft = document.getElementById("arrow-left");
    const arrowRight = document.getElementById("arrow-right");
    let margin = 0;

    const checkMargin = () => {
        margin = Math.max(0, Math.min(margin, logos.offsetWidth - logosWrap.offsetWidth));
        logos.style.marginLeft = "-" + margin + "px";
    };

    arrowRight.addEventListener("click", () => {
        margin = margin + MARGIN_MOVE;
        checkMargin();
    });

    arrowLeft.addEventListener("click", () => {
        margin = margin - MARGIN_MOVE;
        checkMargin();
    });

    window.addEventListener("resize", () => {
        checkMargin();
    })
};

