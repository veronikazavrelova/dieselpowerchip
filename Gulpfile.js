const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const run = require('gulp-run');
const cleanCSS = require('gulp-clean-css');

gulp.task('css', () => {
    gulp.src('src/sass/style.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css/'))
});

gulp.task('js', function () {
    run('npm run build-js').exec();
});

//Watch task
gulp.task('watch', () => {
    gulp.watch('src/sass/**/*.scss', ['css']);
    gulp.watch('src/js/**/*.js', ['js']);
});
